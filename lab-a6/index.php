<?php
    if (isset($_POST['A'])){ // если из формы были переданы данные
        $_POST['A'] = str_replace(',', '.', $_POST['A']);
        $_POST['B'] = str_replace(',', '.', $_POST['B']);
        $_POST['C'] = str_replace(',', '.', $_POST['C']);

        if ($_POST['TASK'] == 'square'){ // Площадь треугольника
            $p = ($_POST['A'] + $_POST['B'] + $_POST['C']) / 2;
            $result = round(($p * ($p - $_POST['A']) * ($p - $_POST['B']) * ($p - $_POST['C'])) ** 0.5, 2);
        } else if( $_POST['TASK'] == 'perimeter' ) { // Периметр треульгоника
            $result = $_POST['A'] + $_POST['B'] + $_POST['C'];
        } else if ($_POST['TASK'] == 'volume'){ // Объем паралеллипипеда
            $result = $_POST['A'] * $_POST['B'] * $_POST['C'];
        } else if ($_POST['TASK'] == 'mean'){ // Среднее арифметическое
            $result = round(($_POST['A'] + $_POST['B'] + $_POST['C']) / 3, 2);
        } else if ($_POST['TASK'] == 'multi'){
            $result = $_POST['A'] * $_POST['B'] * $_POST['C'];
        } else if ($_POST['TASK'] == 'sum'){
            $result = $_POST['A'] + $_POST['B'] + $_POST['C'];
        }
    }
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Хоцевич М. В. 181-322 lab-a6</title>
</head>
<body>
    <header>

    </header>
    <main>
        <?php
            if (isset($result)){ // если форма была обработана
                $out_text = 'ФИО: '.$_POST['FIO'].'<br>'; // подготавливаем содержимое отчета
                $out_text .= 'Группа: '.$_POST['GROUP'].'<br>';

                if($_POST['ABOUT'])
                    $out_text .= $_POST['ABOUT'].'<br>';

                $out_text .= 'Решаемая задача: ';
                if ($_POST['TASK'] == 'square'){
                    $out_text .= 'ПЛОЩАДЬ ТРЕУГОЛЬНИКА';
                } else if ($_POST['TASK'] == 'perimeter'){
                    $out_text .= 'ПЕРИМЕТР ТРЕУГОЛЬНИКА';
                } else if ($_POST['TASK'] == 'volume'){
                    $out_text .= 'ОБЪЕМ ПАРАЛЛЕЛЕПИПЕДА';
                } else if ($_POST['TASK'] == 'mean'){
                    $out_text .= 'СРЕДНЕЕ АРИФМЕТИЧЕСКОЕ';
                } else if ($_POST['TASK'] == 'multi'){
                    $out_text .= 'ПРОИЗВЕДЕНИЕ ЧИСЕЛ';
                } else if ($_POST['TASK'] == 'sum'){
                    $out_text .= 'СУММА ЧИСЕЛ';
                }

                $out_text .= '<br>Значение A: '.$_POST['A'];
                $out_text .= '<br>Значение B: '.$_POST['B'];
                $out_text .= '<br>Значение C: '.$_POST['C'];

                $_POST['result'] = str_replace(',', '.', $_POST['result']);
                $out_text .= '<br>Ваш ответ: '.$_POST['result'];
                if (!is_nan($result) || is_infinite($result)) {
                    $out_text .= '<br>Правильный ответ: '.$result;
                } else {
                    $out_text .= '<br>Правильный ответ: невозможно вычислить';
                }
                
                if ($result == $_POST['result']){
                    $out_text .= '<br><b>ТЕСТ ПРОЕДЕН</b><br>';
                } else {
                    $out_text .= '<br><b>ОШИБКА: ТЕСТ НЕ ПРОЙДЕН!</b><br>';
                }

                echo $out_text; // выводим отчет в браузер

                if (array_key_exists('send_mail', $_POST)){ // если нужно отправить результаты
                    // отправляем результаты по почте простым письмом
                    mail($_POST['MAIL'], 'Результат тестирования', str_replace('<br>', "\r\n", $out_text),
                        "From: auto@mami.ru\n"."Content-Type: text/plain; charset=utf-8\n");
                    // выводим соответствующее сообщение в браузер
                    echo 'Результаты теста были автоматически отправлены на e-mail '.$_POST['MAIL'].'<br>';
                }
                if ($_POST['version'] == 'browser'){
                    echo '<button id="back_button"><a href="?F='.$_POST['FIO'].'&G='.$_POST['GROUP'].'">Повторить тест</a></button>';
                }

            } else { // если форма не обработана (данные не переданы в РНР)
                echo '<form name="form" method="post" action="'.$_SERVER['PHP_SELF'].'">'; // выводим форму
                echo '<label>ФИО: <input type="text" name="FIO" placeholder="Хоцевич Максим Васильевич" value="'.$_GET['F'].'"></label>';
                echo '<label>Номер группы: <input type="text" name="GROUP" placeholder="181-322" value="'.$_GET['G'].'"></label>';
                echo '<label>Найти: 
                    <select name="TASK">
                        <option value="square">Площадь треугольника</option>
                        <option value="perimeter">Периметр треугольника</option>
                        <option value="volume">Объем паралеллипипеда</option>
                        <option value="mean">Среднее арифметическое</option>
                        <option value="multi">Произведение чисел</option>
                        <option value="sum">Сумма чисел</option>
                    </select></label>';
                echo '<label>Значение А: <input type="text" name="A" value="'.(mt_rand(0, 10000) / 100).'" required></label>';
                echo '<label>Значение B: <input type="text" name="B" value="'.(mt_rand(0, 10000) / 100).'" required></label>';
                echo '<label>Значение C: <input type="text" name="C" value="'.(mt_rand(0, 10000) / 100).'" required></label>';
                echo '<label>Ваш ответ: <input type="text" name="result" required></label>';
                echo '<label id="checkbox">Отправить по EMAIL: <input type="checkbox" name="send_mail" 
                    onClick="obj=document.getElementById('."'email'".');
                    if (this.checked){
                        obj.style.display='."'flex'".';
                    }
                    else {
                        obj.style.display='."'none'".';
                    }"></label>';
                echo '<label id="email">E-MAIL: <input type="email" name="MAIL"></label>';
                echo '<label>Немного о себе: <textarea name="ABOUT"></textarea></label>';
                echo '<label>Версия просмотра: 
                    <select name="version">
                        <option value="browser">для браузера</option>
                        <option value="print">для печати</option>
                    </select></label>';
                echo '<input type="submit" value="Проверить">';
                echo '</form>';
            }
            ?>
    </main>
    <footer>

    </footer>
</body>
</html>