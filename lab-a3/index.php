<?php
$counter = isset($_COOKIE["counter"]) ? $_COOKIE["counter"] : 0;
$counter++;
setcookie("counter", $counter);
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-a3</title>
</head>
<body>
    <header>

    </header>
    <main>
        <div class="container">
            <?php
                if( !isset($_GET['store'])) {
                    $_GET['store'] = '';
                } else {
                    if( isset($_GET['key']) ) {
                        $_GET['store'].= $_GET['key'];
                    }
                    
                }
                echo '<div class="result">'.$_GET['store'].'</div>';
            ?>
            <div class="keys">
                <div class="topKeys">
                    <a href="?key=0&store=<?php echo($_GET['store']) ?>">0</a>
                    <a href="?key=1&store=<?php echo($_GET['store']) ?>">1</a>
                    <a href="?key=2&store=<?php echo($_GET['store']) ?>">2</a>
                    <a href="?key=3&store=<?php echo($_GET['store']) ?>">3</a>
                    <a href="?key=4&store=<?php echo($_GET['store']) ?>">4</a>
                </div>
                <div class="bottomKeys">
                    <a href="?key=5&store=<?php echo($_GET['store']) ?>">5</a>
                    <a href="?key=6&store=<?php echo($_GET['store']) ?>">6</a>
                    <a href="?key=7&store=<?php echo($_GET['store']) ?>">7</a>
                    <a href="?key=8&store=<?php echo($_GET['store']) ?>">8</a>
                    <a href="?key=9&store=<?php echo($_GET['store']) ?>">9</a>
                </div>
                <a href="?" class="reset">СБРОС</a>
            </div>
        </div>
    </main>
    <footer>
        <div class="container">
            <?php
                echo $counter;
            ?>
        </div>
    </footer>
</body>
</html>