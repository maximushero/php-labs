<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-a8</title>
</head>
<body>
    <header>

    </header>
    <main>
        <div class="container">
            <?php
                if (isset($_POST['data']) && $_POST['data']){ // если передан текст для анализа
                    echo '<div class="row mt-2 mb-2"><div class="col-12"><div class="text-info"><em>'.$_POST['data'].'</em></div></div></div>'; // выводим текст
                    test_it(iconv('UTF-8', 'cp1251', $_POST['data'])); // анализируем текст
                } else { // если текста нет или он пустой
                    echo '<div class="row mt-2 mb-2"><div class="col-12"><div class="h3 text-danger text-center">Нет текста для анализа</div></div></div>'; // выводим ошибку
                }
                

                function test_it($text){
                    $cifra = array('0' => true, '1' => true, '2' => true, '3' => true, '4' => true, '5' => true,
                                   '6' => true, '7' => true, '8' => true, '9' => true);

                    $marks = array('.' => true, ',' => true, '!' => true, '?' => true, ':' => true, ';' => true,
                                   '-' => true, '(' => true, ')' => true, '"' => true, "'" => true,'/' => true);

                    // вводим переменные для хранения информации о:
                    $cifra_amount = 0; // количество цифр в тексте
                    $marks_amount = 0; // количество знаков
                    $word_amount = 0; // количество слов в тексте
                    $upper_amount = 0; // количество заглавных букв
                    $lower_amount = 0; // количество строчных букв
                    $symbols_amount = 0; // количество символов
                    $words_amount = 0;
                    $print = '';
                    $printWord = '';
                    $word = ''; // текущее слово
                    $words = array(); // список всех слов

                    for ($i = 0; $i < strlen($text); $i++){ // перебираем все символы текста

                        if (array_key_exists($text[$i], $cifra)) // если встретилась цифра
                            $cifra_amount++; // увеличиваем счетчик цифр

                        if (array_key_exists($text[$i], $marks)) // если встретился знак
                            $marks_amount++; // увеличиваем счетчик знаков

                        if (($text[$i] == mb_strtoupper($text[$i], 'cp1251')) && !array_key_exists($text[$i], $cifra) && !array_key_exists($text[$i], $marks) && $text[$i] != ' ' && !preg_match('/\n/', $text[$i]) && !preg_match('/\r/', $text[$i])){
                            $upper_amount++;
                        }

                        // если в тексте встретился пробел или текст закончился
                        if ($text[$i] == ' ' || array_key_exists($text[$i], $marks) || $i == strlen($text) - 1){
                            if ($word){ // если есть текущее слово
                                // если текущее слово сохранено в списке слов
                                if (isset($words[$word]))
                                    $words[$word]++; // увеличиваем число его повторов
                                else
                                    $words[$word] = 1; // первый повтор слова
                            }
                            $word = ''; // сбрасываем текущее слово
                        } else // если слово продолжается
                            $word .= $text[$i]; //добавляем в текущее слово новый символ
                    }

                   
                    $arrSymbs = test_symbs($text);

                    foreach ($arrSymbs as $key => $value) {
                        $key = iconv('cp1251', 'UTF-8', $key);
                        $print .= "$key => $value<br>";
                        $symbols_amount += $value;
                    }
                    ksort($words);
                    foreach ($words as $key => $value) {
                        $key = iconv('cp1251', 'UTF-8', $key);
                        $printWord .= "$key => $value<br>";
                        $words_amount += $value;
                    }
                   
                    echo "<table>";
                    echo '<tr><td>Количество символов: '.strlen($text).'</td></tr>'; // количество символов в тексте определяется 
                    echo '<tr><td>Количество букв: '.iconv('cp1251', 'UTF-8', $symbols_amount).'</td></tr>';
                    echo '<tr><td>Количество строчных букв: '.iconv('cp1251', 'UTF-8', $symbols_amount - $upper_amount).'</td></tr>';
                    echo '<tr><td>Количество заглавных букв: '.iconv('cp1251', 'UTF-8', $upper_amount).'</td></tr>';
                    echo '<tr><td>Количество знаков препинания: '.iconv('cp1251', 'UTF-8', $marks_amount).'</td></tr>';
                    echo '<tr><td>Количество цифр: '.iconv('cp1251', 'UTF-8', $cifra_amount).'</td></tr>';
                    echo '<tr><td>Количество слов: '.iconv('cp1251', 'UTF-8', $words_amount).'</td></tr>';
                    echo "<tr><td>Количество вхождений каждого символа текста:<br>$print</td></tr>";
                    echo "<tr><td>Cписок всех слов в тексте и количество их вхождений:<br>$printWord</td></tr>";
                    echo "</table>";
                    echo "<a href='index.html' class='btn btn-primary mt-2'>Другой анализ</a>";
                }

                function test_symbs($text){
                    $symbs = array(); // массив символов текста
                    $l_text = mb_strtolower($text, 'cp1251'); // переводим текст в нижний регистр последовательно перебираем все символы текста
                    $cifra = array('0' => true, '1' => true, '2' => true, '3' => true, '4' => true, '5' => true,
                                   '6' => true, '7' => true, '8' => true, '9' => true);
                    $marks = array('.' => true, ',' => true, '!' => true, '?' => true, ':' => true, ';' => true,
                                   '-' => true, '(' => true, ')' => true, '"' => true, "'" => true);
                    for ($i = 0; $i < strlen($l_text); $i++){

                        if (!array_key_exists($l_text[$i], $marks) && !array_key_exists($l_text[$i], $cifra) && $l_text[$i] != ' ' && !preg_match('/\n/', $text[$i]) && !preg_match('/\r/', $text[$i])){
                            if (isset($symbs[$l_text[$i]])) // если символ есть в массиве
                                $symbs[$l_text[$i]]++; // увеличиваем счетчик повторов
                            else // иначе
                                $symbs[$l_text[$i]] = 1; // добавляем символ в массив
                        }      
                    }
                    return $symbs; // возвращаем массив с числом вхождений символов в тексте
                }

            ?>
        </div>
    </main>
    <footer>

    </footer>
</body>
</html>