<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-b2</title>
</head>
<body>
    <header></header>
    <main>
        <div class="container">
            <form method="POST" action="<?php echo($_SERVER['PHP_SELF']) ?>">
                <input type="text" name="val"><br>
                <input class="btn btn-primary" type="submit" name="button">
            </form>
            <?php
                if (isset($_POST['val'])) { // если передан POST-параметр val
                    $res = calculate($_POST['val']); // вычисляем результат выражения
                    if (isnum($res)) // если полученный результат является числом
                        echo 'Значение выражения: '.$res; // вывод значения
                    else // если результат не число – значит ошибка!
                        echo 'Ошибка вычисления выражения: '.$res; // вывод ошибки
                }

                function calculate($val) {
                    if (!$val) return 'Выражение не задано!'; // если строка пуста – ошибка
                    if (isnum($val)) return $val; // если выражение число – возвращаем его

                    // разбиваем строку на аргументы и заносим их в массив
                    $args = explode('+', $val);
                    if (count($args) > 1) { // если в выражении есть символы «+»
                        $sum = 0; // начальное значение суммы аргументов
                        for ($i = 0; $i < count($args); $i++) { // перебираем все слагаемые
                            $arg = calculate($args[$i]); // вычисляем значение слагаемого
                            if (!isnum($arg)) return $arg; // если результат не число возвращаем ошибку
                            $sum += $arg; // суммируем слагаемое с предыдущими
                        }
                        return $sum; // если все слагаемые числа – возвращаем сумму
                    }

                    // // разбиваем строку на множители и заносим их в массив
                    // $args = explode('*', $val);
                    // if (count($args) > 1) { // если в выражении есть символы «*»
                    //     $sup = 1; // начальное значение произведения аргументов
                    //     for ($i = 0; $i < count($args); $i++) { // перебираем все множители
                    //         $arg = $args[$i]; // текущий множитель
                    //         // проверяем – если множитель не число –возвращаем ошибку
                    //         if (!isnum($arg)) return 'Неправильная форма числа!';
                    //         $sup *= $arg; // умножаем множитель с предыдущими
                    //     }
                    //     return $sup; // если все множители числа – возвращаем произведение
                    // }

                    // // разбиваем строку на множители и заносим их в массив
                    // $args = explode('*', $val);
                    // if (count($args) > 1) { // если в выражении есть символы «*»
                    //     $sup = 1; // начальное значение произведения аргументов
                    //     for ($i = 0; $i < count($args); $i++) { // перебираем все множители
                    //         $arg = $args[$i]; // текущий множитель
                    //         // проверяем – если множитель не число –возвращаем ошибку
                    //         if (!isnum($arg)) return 'Неправильная форма числа!';
                    //         $sup *= $arg; // умножаем множитель с предыдущими
                    //     }
                    //     return $sup; // если все множители числа – возвращаем произведение
                    // }



                    // выражение – не число, но и символов «+» или в нем нет
                    return 'Недопустимые символы в выражении';
                }

                function isnum($x) {
                    if (!$x) return false; // если строка пустая – это НЕ число!
                    if ($x[0] == '.' || $x[0] == '0') return false; // число не может начинаться с точки или нуля
                    if ($x[strlen($x)-1] == '.' ) return false; // число не может заканчиваться на точку

                    // перебираем все символы строки в цикле
                    for($i = 0, $point_count = false; $i < strlen($x); $i++){
                        // если в проверяемой строке недопустимый в числе символ
                        if ($x[$i] != '0' && $x[$i] != '1' && $x[$i] != '2' && $x[$i] != '3' && $x[$i] != '4' && $x[$i] != '5' && $x[$i] != '6' && $x[$i] != '7' && $x[$i] != '8' && $x[$i] != '9' && $x[$i] != '.') return false; // недопустимые символы в строке
                        if ($x[$i] == '.') { // если в строке встретилась точка
                            if ($point_count) // если точка уже встречалась
                                return false; // то это не число
                            else // если это первая точка в строке
                                $point_count = true; // запоминаем это
                        }
                    }
                    return true; // все проверки пройдены – это число
                }
            ?>
        </div>
    </main>
    <footer></footer>
</body>
</html>