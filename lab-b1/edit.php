<?php
$mysqli = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_927', 'bSjwWLdxmGIT', 'std_927');
if (mysqli_connect_errno()){ // если при подключении к серверу произошла ошибка выводим сообщение и принудительно останавливаем РНР-программу
	echo 'Ошибка подключения к БД: '.mysqli_connect_error();
	exit();
}
// если были переданы данные для изменения записи в таблице
if (isset($_POST['button']) && $_POST['button'] == 'Изменить запись'){
	// формируем и выполняем SQL-запрос на изменение записи с указанным id
	$sql = 'UPDATE friends SET';
	$sql .= '`lastname`="'.htmlspecialchars($_POST['lastname']).'", ';
	$sql .= '`name`="'.htmlspecialchars($_POST['name']).'", ';
	$sql .= '`surname`="'.htmlspecialchars($_POST['surname']).'", ';
	$sql .= '`sex`="'.htmlspecialchars($_POST['sex']).'", ';
	$sql .= '`dob`="'.htmlspecialchars($_POST['dob']).'", ';
	$sql .= '`telephone`="'.htmlspecialchars($_POST['telephone']).'", ';
	$sql .= '`address`="'.htmlspecialchars($_POST['address']).'", ';
	$sql .= '`mail`="'.htmlspecialchars($_POST['mail']).'", ';
	$sql .= '`comment`="'.htmlspecialchars($_POST['comment']).'" WHERE `id`='.htmlspecialchars($_GET['id']);
	$sql_res = mysqli_query($mysqli, $sql);
	echo '<div class="dataEdit">Данные изменены</div>'; // и выводим сообщение об изменении данных
}

$currentROW = array(); // информации о текущей записи пока нет
// если id текущей записи передано –
if (isset($_GET['id'])){ // (переход по ссылке или отправка формы)
	// выполняем поиск записи по ее id
	$sql_res = mysqli_query($mysqli, 'SELECT * FROM friends WHERE id='.$_GET['id'].' LIMIT 0, 1');
	$currentROW = mysqli_fetch_assoc($sql_res); // информация сохраняется
}

if (!$currentROW){ // если информации о текущей записи нет или она некорректна
	// берем первую запись из таблицы и делаем ее текущей
	$sql_res = mysqli_query($mysqli, 'SELECT * FROM friends ORDER BY lastname LIMIT 0, 1');
	$currentROW = mysqli_fetch_assoc($sql_res);
}
// формируем и выполняем запрос для получения требуемых полей всех записей таблицы
$sql_res = mysqli_query($mysqli, 'SELECT `id`, `lastname`, `name` FROM friends ORDER BY lastname');

if (!mysqli_errno($mysqli)){ // если запрос успешно выполнен
	echo '<div id="edit_links">';
	while ($row = mysqli_fetch_assoc($sql_res)){ // перебираем все записи выборки
		// если текущая запись пока не найдена и ее id не передан
		// или передан и совпадает с проверяемой записью
		if ($currentROW['id'] == $row['id']){
			echo '<div>'.$row['lastname'].' '.$row['name'].'</div>'; // и выводим ее в списке
		} else { // если проверяемая в цикле запись не текущая формируем ссылку на нее
			echo '<a class="editA" href="'.$_SERVER['PHP_SELF'].'?p=edit&id='.$row['id'].'">'.$row['lastname'].' '.$row['name'].'</a>';
		}
	}
	echo '</div>';
	if ($currentROW){ // если есть текущая запись, т.е. если в таблице есть записи
		// формируем HTML-код формы
		echo '<form name="form_edit" method="post" action="'.$_SERVER['PHP_SELF'].'?p=edit&id='.$currentROW['id'].'">
		<input type="text" name="lastname" id="lastname" value="'.$currentROW['lastname'].'"><br>
		<input type="text" name="name" id="name" value="'.$currentROW['name'].'"><br>
		<input type="text" name="surname" id="surname" value="'.$currentROW['surname'].'"><br>
		<input type="text" name="sex" id="sex" value="'.$currentROW['sex'].'"><br>
		<input type="text" name="dob" id="dob" value="'.$currentROW['dob'].'"><br>
		<input type="text" name="telephone" id="telephone" value="'.$currentROW['telephone'].'"><br>
		<input type="text" name="address" id="address" value="'.$currentROW['address'].'"><br>
		<input type="text" name="mail" id="mail" value="'.$currentROW['mail'].'"><br>
		<textarea name="comment" id="comment">'.$currentROW['comment'].'</textarea><br>
		<input class="btn btn-primary" type="submit" name="button" value="Изменить запись"></form>';
	} else {
		echo 'Записей пока нет';
	}
} else // если запрос не может быть выполнен
	echo 'Ошибка базы данных'; // выводим сообщение об ошибке

// $mysqli = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_927', 'bSjwWLdxmGIT', 'std_927');
// if (mysqli_connect_errno()){ // если при подключении к серверу произошла ошибка
// 	// выводим сообщение и принудительно останавливаем РНР-программу
// 	echo 'Ошибка подключения к БД: '.mysqli_connect_error();
// 	exit();
// }
// // если были переданы данные для изменения записи в таблице
// if (isset($_POST['button']) && $_POST['button'] == 'Изменить запись'){
// 	// формируем и выполняем SQL-запрос на изменение записи с указанным id
// 	$sql = 'UPDATE friends SET';
// 	$sql .= '`lastname`="'.htmlspecialchars($_POST['lastname']).'", ';
// 	$sql .= '`name`="'.htmlspecialchars($_POST['name']).'", ';
// 	$sql .= '`surname`="'.htmlspecialchars($_POST['surname']).'", ';
// 	$sql .= '`sex`="'.htmlspecialchars($_POST['sex']).'", ';
// 	$sql .= '`dob`="'.htmlspecialchars($_POST['dob']).'", ';
// 	$sql .= '`telephone`="'.htmlspecialchars($_POST['telephone']).'", ';
// 	$sql .= '`address`="'.htmlspecialchars($_POST['address']).'", ';
// 	$sql .= '`mail`="'.htmlspecialchars($_POST['mail']).'", ';
// 	$sql .= '`comment`="'.htmlspecialchars($_POST['comment']).'" WHERE `id`='.htmlspecialchars($_POST['id']);

// 	$sql_res = mysqli_query($mysqli, $sql);
// 	echo 'Данные изменены'; // и выводим сообщение об изменении данных
// 	$_GET['id'] = $_POST['id']; // эмулируем переход по ссылке на изменяемую запись
// }
// // формируем и выполняем запрос для получения требуемых полей всех записей таблицы
// $sql_res = mysqli_query($mysqli, 'SELECT * FROM friends');

// if (!mysqli_errno($mysqli)){ // если запрос успешно выполнен
// 	$currentROW = array(); // создаем массив для хранения текущей записи
// 	$ret = '<table><tr><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Пол</th><th>Дата рождения</th><th>Телефон</th><th>Адрес</th><th>E-mail</th><th>Комментарий</th></tr>';
// 	while ($row = mysqli_fetch_assoc($sql_res)){ // перебираем все записи выборки если текущая запись пока не найдена и ее id не передан или передан и совпадает с проверяемой записью
// 		if (!$currentROW && (!isset($_GET['id']) || $_GET['id'] == $row['id'])){
// 			// значит в цикле сейчас текущая запись
// 			$currentROW = $row; // сохраняем информацию о ней в массиве
// 			// выводим каждую запись как строку таблицы
// 			$ret .= '<tr><td>'.$row['lastname'].'</td>
// 					<td>'.$row['name'].'</td>
// 					<td>'.$row['surname'].'</td>
// 					<td>'.$row['sex'].'</td>
// 					<td>'.$row['dob'].'</td>
// 					<td>'.$row['telephone'].'</td>
// 					<td>'.$row['address'].'</td>
// 					<td>'.$row['mail'].'</td>
// 					<td>'.$row['comment'].'</td>
// 					<td>Изменить</td></tr>';
			
// 		} else { // если проверяемая в цикле запись не текущая
// 			$ret .= '<tr><td>'.$row['lastname'].'</td>
// 					<td>'.$row['name'].'</td>
// 					<td>'.$row['surname'].'</td>
// 					<td>'.$row['sex'].'</td>
// 					<td>'.$row['dob'].'</td>
// 					<td>'.$row['telephone'].'</td>
// 					<td>'.$row['address'].'</td>
// 					<td>'.$row['mail'].'</td>
// 					<td>'.$row['comment'].'</td>
// 					<td><a href="'.$_SERVER['PHP_SELF'].'?p=edit&id='.$row['id'].'">Изменить</a></td></tr>';
// 		}
// 	}
// 	$ret .= '</table>'; // заканчиваем формирование таблицы с контентом
// 	echo $ret;

// 	// формируем HTML-код формы
// 	echo '<form name="form_edit" method="post" action="'.$_SERVER['PHP_SELF'].'?p=edit">
// 		<input type="text" name="lastname" id="lastname"';
// 	// если текущая запись определена – устанавливаем значение полей формы
// 	if ($currentROW) echo ' value="'.$currentROW['lastname'].'"><br>';

// 	echo '<input type="text" name="name" id="name"';
// 	if ($currentROW) echo ' value="'.$currentROW['name'].'"><br>';

// 	echo '<input type="text" name="surname" id="surname"';
// 	if ($currentROW) echo ' value="'.$currentROW['surname'].'"><br>';

// 	echo '<input type="text" name="sex" id="sex"';
// 	if ($currentROW) echo ' value="'.$currentROW['sex'].'"><br>';

// 	echo '<input type="text" name="dob" id="dob"';
// 	if ($currentROW) echo ' value="'.$currentROW['dob'].'"><br>';

// 	echo '<input type="text" name="telephone" id="telephone"';
// 	if ($currentROW) echo ' value="'.$currentROW['telephone'].'"><br>';

// 	echo '<input type="text" name="address" id="address"';
// 	if ($currentROW) echo ' value="'.$currentROW['address'].'"><br>';

// 	echo '<input type="text" name="mail" id="mail"';
// 	if ($currentROW) echo ' value="'.$currentROW['mail'].'"><br>';

// 	echo '<textarea name="comment" id="comment">';
// 	if ($currentROW) echo $currentROW['comment'].'</textarea><br>';

// 	echo '<input type="submit" name="button" value="Изменить запись"><input type="hidden" name="id" value="';
// 	if ($currentROW) // если текущая запись определена
// 		echo $currentROW['id']; // передаем ее id как POST параметр формы
// 	echo '"></form>';
// } else // если запрос не может быть выполнен
// 	echo 'Ошибка базы данных'; // выводим сообщение об ошибке
?>