<form name="form_add" method="post" action="<?php echo($_SERVER['PHP_SELF'])?>?p=add">
	<input type="text" name="lastname" id="lastname" placeholder="Фамилия"><br>
	<input type="text" name="name" id="name" placeholder="Имя"><br>
	<input type="text" name="surname" id="surname" placeholder="Отчество"><br>
	<input type="text" name="sex" id="sex" placeholder="Пол"><br>
	<input type="text" name="dob" id="dob" placeholder="Дата рождения"><br>
	<input type="text" name="telephone" id="telephone" placeholder="Телефон"><br>
	<input type="text" name="address" id="address" placeholder="Адрес"><br>
	<input type="text" name="mail" id="mail" placeholder="E-mail"><br>
	<textarea name="comment" placeholder="Краткий комментарий"></textarea><br>
	<input class="btn btn-primary" type="submit" name="button" value="Добавить запись">
</form>
<?php
// если были переданы данные для добавления в БД
if (isset($_POST['button']) && $_POST['button'] == 'Добавить запись'){
	$mysqli = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_927', 'bSjwWLdxmGIT', 'std_927');
	if (mysqli_connect_errno()) // проверяем корректность подключения
		echo 'Ошибка подключения к БД: '.mysqli_connect_error();

	// формируем и выполняем SQL-запрос для добавления записи
	$sql = 'INSERT INTO friends (`lastname`, `name`, `surname`, `sex`, `dob`, `telephone`, `address`, `mail`, `comment`) VALUES';
	$sql .= '("'.htmlspecialchars($_POST['lastname']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['name']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['surname']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['sex']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['dob']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['telephone']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['address']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['mail']).'"';
	$sql .= ', "'.htmlspecialchars($_POST['comment']).'")';

	$sql_res = mysqli_query($mysqli, $sql);
	// если при выполнении запроса произошла ошибка – выводим сообщение
	if (mysqli_errno($mysqli))
		echo '<div class="error">Запись не добавлена</div>';
	else // если все прошло нормально – выводим сообщение
		echo '<div class="ok">Запись добавлена</div>';
}
?>