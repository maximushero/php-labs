<?php
function getFriendsList($type, $page){
	$mysqli = mysqli_connect('std-mysql.ist.mospolytech.ru', 'std_927', 'bSjwWLdxmGIT', 'std_927'); // осуществляем подключение к базе данных
	if (mysqli_connect_errno()){ // проверяем корректность подключения
		return 'Ошибка подключения к БД: '.mysqli_connect_error();
	}

	// формируем и выполняем SQL-запрос для определения числа записей
	$sql_res = mysqli_query($mysqli, 'SELECT COUNT(*) AS `TOTAL` FROM friends');

	// проверяем корректность выполнения запроса и определяем его результат
	if (!mysqli_errno($mysqli) && $row = mysqli_fetch_row($sql_res)){
		if (!$TOTAL = $row[0]){ // если в таблице нет записей
			return 'В таблице нет данных'; // возвращаем сообщение
		}
		$PAGES = ceil($TOTAL / 10); // вычисляем общее количество страниц
		if ($page >= $PAGES) // если указана страница больше максимальной
			$page = $PAGES - 1; // будем выводить последнюю страницу

		// формируем и выполняем SQL-запрос для выборки записей из БД
		if ($type == 'byid') {
			$sql = 'SELECT * FROM `friends` ORDER BY `id` LIMIT '.($page * 10).', 10';
		} else if ($type == 'fam'){
			$sql = 'SELECT * FROM `friends` ORDER BY `lastname` LIMIT '.($page * 10).', 10';
		} else if ($type == 'birth'){
			$sql = 'SELECT * FROM `friends` ORDER BY `dob` LIMIT '.($page * 10).', 10';
		}

		
		$sql_res = mysqli_query($mysqli, $sql);

		$ret = '<table><tr><th>Фамилия</th><th>Имя</th><th>Отчество</th><th>Пол</th><th>Дата рождения</th><th>Телефон</th><th>Адрес</th><th>E-mail</th><th>Комментарий</th></tr>'; // строка с будущим контентом страницы
		while ($row = mysqli_fetch_assoc($sql_res)){ // пока есть записи
			// выводим каждую запись как строку таблицы
			$ret .= '<tr><td>'.$row['lastname'].'</td>
					<td>'.$row['name'].'</td>
					<td>'.$row['surname'].'</td>
					<td>'.$row['sex'].'</td>
					<td>'.$row['dob'].'</td>
					<td>'.$row['telephone'].'</td>
					<td>'.$row['address'].'</td>
					<td>'.$row['mail'].'</td>
					<td>'.$row['comment'].'</td></tr>';
		}
		$ret .= '</table>'; // заканчиваем формирование таблицы с контентом
		if ($PAGES > 1){ // если страниц больше одной – добавляем пагинацию
			$ret .= '<div id="pages">'; // блок пагинации
			for ($i = 0; $i < $PAGES; $i++) // цикл для всех страниц пагинации
				if ($i != $page) // если не текущая страница
					$ret .= '<a class="btn btn-primary btn-sm mr-2 ml-2" href="?p=viewer&sort='.$_GET['sort'].'&pg='.$i.'">'.($i+1).'</a>';
				else // если текущая страница
					$ret .= '<span class="btn btn-danger btn-sm mr-2 ml-2">'.($i+1).'</span>';
			$ret.='</div>';
		}
		return $ret; // возвращаем сформированный контент
	}
	// если запрос выполнен некорректно
	return 'Неизвестная ошибка'; // возвращаем сообщение
}
?>