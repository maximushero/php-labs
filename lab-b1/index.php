<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-a2</title>
</head>
<body>
    <header></header>
    <main>
        <div class="container">
            <?php
                require 'menu.php'; // главное меню
                if ($_GET['p'] == 'viewer'){ // если выбран пункт меню "Просмотр"
                    include 'viewer.php'; // подключаем модуль с библиотекой функций
                    // если в параметрах не указана текущая страница – выводим самую первую
                    if (!isset($_GET['pg']) || $_GET['pg'] < 0) $_GET['pg'] = 0;
                    // если в параметрах не указан тип сортировки или он недопустим
                    if (!isset($_GET['sort']) || ($_GET['sort'] != 'byid' && $_GET['sort'] != 'fam' && $_GET['sort'] != 'birth'))
                        $_GET['sort'] = 'byid'; // устанавливаем сортировку по умолчанию
                    // формируем контент страницы с помощью функции и выводим его
                    echo getFriendsList($_GET['sort'], $_GET['pg']);
                } else // подключаем другие модули с контентом страницы
                    if (file_exists($_GET['p'].'.php')){
                        include $_GET['p'].'.php';
                    }
            ?>  
        </div> 
    </main>
    <footer></footer>
</body>
</html>