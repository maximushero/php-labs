<div id="menu">
	<?php
		if (!isset($_GET['p'])){
			$_GET['p'] = 'viewer'; // если нет параметра меню – добавляем его
		} 
		if ($_GET['p'] != 'viewer' && $_GET['p'] != 'add' && $_GET['p'] != 'edit' && $_GET['p'] != 'delete'){
			exit();
		}

		echo '<a href="'.$_SERVER['PHP_SELF'].'?p=viewer" class="btn btn-primary mr-2'; // первый пункт меню
		if ($_GET['p'] == 'viewer') // если он выбран, выделяем его
			echo ' btn-danger"';
		else
			echo '"'; 
		echo '>Просмотр</a>';

		echo '<a href="'.$_SERVER['PHP_SELF'].'?p=add" class="btn btn-primary mr-2'; // первый пункт меню
		if ($_GET['p'] == 'add') // если он выбран, выделяем его
			echo ' btn-danger"';
		else
			echo '"'; 
		echo '>Добавление записи</a>';

		echo '<a href="'.$_SERVER['PHP_SELF'].'?p=edit" class="btn btn-primary mr-2'; // первый пункт меню
		if ($_GET['p'] == 'edit') // если он выбран, выделяем его
			echo ' btn-danger"';
		else
			echo '"'; 
		echo '>Редактирование записи</a>';

		echo '<a href="'.$_SERVER['PHP_SELF'].'?p=delete" class="btn btn-primary'; // первый пункт меню
		if ($_GET['p'] == 'delete') // если он выбран, выделяем его
			echo ' btn-danger"';
		else
			echo '"'; 
		echo '>Удаление записи</a>';

		if ($_GET['p'] == 'viewer'){ //если был выбран первый пунт меню
			echo '<div id="submenu">'; // выводим подменю
			echo '<a href="'.$_SERVER['PHP_SELF'].'?p=viewer&sort=byid&pg='.$_GET['pg'].'" class="btn btn-sm btn-primary mr-2'; // первый пункт подменю
			if (!isset($_GET['sort']) || $_GET['sort'] == 'byid')
				echo ' btn-danger"';
			else
				echo '"';
			echo '>По-умолчанию</a>';

			echo '<a href="'.$_SERVER['PHP_SELF'].'?p=viewer&sort=fam&pg='.$_GET['pg'].'" class="btn btn-sm btn-primary mr-2'; // второй пункт подменю
			if (isset($_GET['sort']) && $_GET['sort'] == 'fam')
				echo ' btn-danger"';
			else
				echo '"';
			echo '>По фамилии</a>';

			echo '<a href="'.$_SERVER['PHP_SELF'].'?p=viewer&sort=birth&pg='.$_GET['pg'].'" class="btn btn-sm btn-primary mr-2'; // второй пункт подменю
			if (isset($_GET['sort']) && $_GET['sort'] == 'birth')
				echo ' btn-danger"';
			else
				echo '"';
			echo '>По дате рождения</a>';
			echo '</div>'; // конец подменю
		}
	?>
</div>