<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-a7</title>
</head>
<body>
    <header>

    </header>
    <main>
        <script>
            function addElement(table_name){ // функция добавляет еще один элемент
                var t = document.getElementById(table_name); // объект таблицы
                var index = t.rows.length; // индекс новой строки
                var row = t.insertRow(index); // добавляем новую строку
                var cel1 = row.insertCell(0); // добавляем в строку ячейку
                var cel2 = row.insertCell(1); // добавляем в строку ячейку
                cel1.className = 'element_row'; // определяем css-класс ячейки
                cel2.className = 'element_row'; // определяем css-класс ячейки
                var cel1content = `${index}`; // формируем html-код содержимого ячейки
                var cel2content = `<input type="text" name="element${index}">`; // формируем html-код содержимого ячейки
                setHTML(cel1, cel1content); // добавляем контент в ячеку таблицы
                setHTML(cel2, cel2content); // добавляем контент в ячеку таблицы
                document.getElementById('arrLength').value = t.rows.length; // в скрытом поле записываем количество полей (строк таблицы)
            }

            function setHTML(element, txt){
                if(element.innerHTML)
                    element.innerHTML = txt;
                else {
                    var range = document.createRange();
                    range.selectNodeContents(element);
                    range.deleteContents();
                    var fragment = range.createContextualFragment(txt);
                    element.appendChild(fragment);
                }
            }
        </script>
        <form action="sort.php" method="POST" target="_black">
            <select name="algorithm">
                <option value="choiceSort">Сортировка выбором</option>
                <option value="bubbleSort">Сортировка пузырьком</option>
                <option value="shakerSort">Шейкерная сортировка</option>
                <option value="insertSort">Сотировка вставками</option>
                <option value="gnomeSort">Алгоритм садового гнома</option>
                <option value="shellsSort">Алгоритм Шелла</option>
                <option value="quickSort">Быстрая сортировка</option>
                <option value="defaultSort">Встроенная функция РНР</option>
            </select>
            <table id="elements">
                <tr>
                    <td class="element_row">0</td>
                    <td class="element_row"><input type="text" name="element0"></td>
                </tr>
            </table>
            <input type="hidden" id="arrLength" name="arrLength">
            <input type="button" value="Добавить еще один элемент" onClick="addElement('elements');">
            <input type="submit" name="" value="Сортировать массив">
        </form>
    </main>
    <footer>

    </footer>
</body>
</html>