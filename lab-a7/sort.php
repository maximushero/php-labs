<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>Хоцевич М. В. 181-322 lab-a7</title>
</head>
<body>
    <header>

    </header>
    <main>
		<?php
			if (!isset($_POST['element0'])){ // если данных нет
				echo 'Массив не задан, сортировка невозможна'; // сообщение
				exit(); // и завершение программы
			}

			for ($i = 0; $i < $_POST['arrLength']; $i++){// для всех элементов массива
				if (arg_is_not_Num($_POST['element'.$i])){ // если элемент массива не число
					echo 'Элемент массива "'. $_POST['element'.$i].'" – не число'; // выводим сообщение и завершаем программу
					exit();
				}
			}

			// определим реализуемый алгоритм и выведем его название
			if ($_POST['algorithm'] === 'choiceSort')
				echo '<h1>Сортировка выбором</h1>';
			else if ($_POST['algorithm'] === 'bubbleSort')
				echo '<h1>Сортировка пузырьком</h1>';
			else if ($_POST['algorithm'] === 'shakerSort')
				echo '<h1>Шейкерная сортировка</h1>';
			else if ($_POST['algorithm'] === 'insertSort')
				echo '<h1>Сотировка вставками</h1>';
			else if ($_POST['algorithm'] === 'gnomeSort')
				echo '<h1>Алгоритм садового гнома</h1>';
			else if ($_POST['algorithm'] === 'shellsSort')
				echo '<h1>Алгоритм Шелла</h1>';
			else if ($_POST['algorithm'] === 'quickSort')
				echo '<h1>Быстрая сортировка</h1>';
			else if ($_POST['algorithm'] === 'defaultSort')
				echo '<h1>Встроенная функция РНР</h1>';



			$arr = array(); // создаем пустой массив для формирования сортируемого списка
			echo 'Исходный массив<br>----------------------------<br>';
			for ($i = 0; $i < $_POST['arrLength']; $i++){ // для всех элементов массива
				echo '<div class="arr_element">'.$i.': '.$_POST['element'.$i].'</div>'; // выводим текущий элемент и его номер
				$arr[] = $_POST['element'.$i]; // добавляем элемент в массив для сортировки
			}
			// сообщение об успешной валидации
			echo '----------------------------<br>Массив проверен, сортировка возможна<br>';
			$time = microtime(true); // засекаем время начала сортировки
			if ($_POST['algorithm'] === 'choiceSort')
				$arr = choiceSort($arr);
			else if ($_POST['algorithm'] === 'bubbleSort')
				$arr = bubbleSort($arr);
			else if ($_POST['algorithm'] === 'shakerSort')
				$arr = shakerSort($arr);
			else if ($_POST['algorithm'] === 'insertSort')
				$arr = insertSort($arr);
			else if ($_POST['algorithm'] === 'gnomeSort')
				$arr = gnomeSort($arr);
			else if ($_POST['algorithm'] === 'shellsSort')
				$arr = shellsSort($arr);
			else if ($_POST['algorithm'] === 'quickSort'){
				$counter = quickSort($arr, 0, count($arr) - 1, 0);
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
			}
			else if ($_POST['algorithm'] === 'defaultSort'){
				sort($arr);
				echo "Значение массива: ";
				foreach ($arr as $key => $value) {
					if ($key == count($arr) - 1){
						echo $value;
						break;
					}
					echo "$value, ";
				}
				echo "<br>";
			}

			// считаем и выводим затраченое на сортировку время
			echo '<br>Затрачено '.(microtime(true) - $time).' микросекунд!';

			function arg_is_not_Num($arg){
				if ($arg === '') return true; // передана пустая строка
				for ($i = 0; $i < strlen($arg); $i++) // цикл по всем символам аргумента
					if ($arg[$i] !== '0' && $arg[$i] !== '1' && $arg[$i] !== '2' && 
						$arg[$i] !== '3' && $arg[$i] !== '4' && $arg[$i] !== '5' && 
						$arg[$i] !== '6' && $arg[$i] !== '7' && $arg[$i] !== '8' && $arg[$i] !== '9') // если встретилась не цифра
						return true; // возвращаем true
				// строка состоит из чисел - возвращаем false
				return false;
			}

			function choiceSort($arr){ // функция сортировки выбором
				$counter = 0;
				for ($i = 0; $i < count($arr) - 1; $i++){ // цикл для всех элементов списка
					$min = $i; // неотсортированной частью массива считаем элементы начиная от текущего
					for ($j = $i + 1; $j < count($arr); $j++){ // ищем минимальный элемент
						if ($arr[$j] < $arr[$min]) $min = $j; // в неотсортированной части
						$counter++;
						printIteration($arr, $counter);
					}
					$element = $arr[$i]; // меняем его с первым
					$arr[$i] = $arr[$min];
					$arr[$min] = $element;
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function bubbleSort($arr){
				$counter = 0;
				for ($j = 0; $j < count($arr) - 1; $j++){ // число повторов: длина массива - 1
				// для всех элементов рассматриваемой части массива от нулевого до предпоследнего
					for ($i = 0; $i < count($arr) - 1 - $j; $i++){
						if ($arr[$i] > $arr[$i+1] ){ // если текущий элемент меньше следующего
							$temp = $arr[$i]; // меняем их местами
							$arr[$i] = $arr[$i+1];
							$arr[$i+1] = $temp;
						}
						$counter++;
						printIteration($arr, $counter);
					}
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function shakerSort($arr){
				$counter = 0;
				$left = 1; // слева-направо с первого элемента массива
				$right = count($arr) - 1; // справа-налево с последнего
				while ($left <= $right){ // пока границы не сойдутся
					// обход аналогично пузырьковому алгоритму с правой границы до левой
					for ($i = $right; $i >= $left; $i--){
						if ($arr[$i-1] > $arr[$i]){ // если предыдущий элемент больше
							$temp = $arr[$i-1]; // меняем его с текущим местами
							$arr[$i-1] = $arr[$i];
							$arr[$i] = $temp;
						}
						$counter++;
						printIteration($arr, $counter);
					}
					$left++; // сдвигаем левую границу вправо

					// обход аналогично пузырьковому алгоритму с левой границы до правой
					for ($i = $left; $i <= $right; $i++){
						if ($arr[$i-1] > $arr[$i]){ // если предыдущий элемент больше
							$temp = $arr[$i-1]; // меняем его с текущим местами
							$arr[$i-1] = $arr[$i];
							$arr[$i] = $temp;
						}
						$counter++;
						printIteration($arr, $counter);
					}
					$right--; // сдвигаем правую границу влево
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function insertSort($arr){
				$counter = 0;
				for ($i = 1; $i < count($arr); $i++){ // для всех элементов начиная с первого
					$val = $arr[$i]; // сохраняем значение текущего элемента
					$j = $i - 1; // начинаем перебор с предыдущего элемента пока не найден элемент меньше текущего
					while ($j >= 0 && $arr[$j] > $val){
						$arr[$j+1] = $arr[$j]; // сдвигаем элементы массива вправо
						$j--;
						$counter++;
						printIteration($arr, $counter);
					}
					$arr[$j+1] = $val; // вставляем текущий элемент на свое место
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function gnomeSort($arr){
				$counter = 0;
				$i = 1; // начинаем со второго элемента массива
				$j = 2;
				while ($i < count($arr)){ // пока не достигнут последний элемент - цикл
					// если первый элемент массива (предыдущего нет)
					// или текущий элемент больше предыдущего
					if (!$i || $arr[$i-1] <= $arr[$i]){
						$i = $j; // возвращаемся к месту
						$j++; // до которого уже дошли
					} else { // иначе
						$temp = $arr[$i]; // меняем элементы местами
						$arr[$i] = $arr[$i-1];
						$arr[$i-1] = $temp;
						$i--; // шагаем назад
					}
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function shellsSort($arr){
				$counter = 0;
				// вычисляем в цикле последовательность расстояний
				for ($k = ceil(count($arr) / 2); $k >= 1; $k = floor($k / 2)){
					for ($i = $k; $i < count($arr); $i++){ // для всех элементов начиная с первого
						$val = $arr[$i]; // сохраняем значение текущего элемента
						$j = $i - $k; // начинаем перебор с элемента с k меньшим индексом
						// пока не найден элемент меньше текущего
						while ($j >= 0 && $arr[$j] > $val){
							$arr[$j+$k] = $arr[$j]; // сдвигаем элементы вправо
							$j -= $k;
							$counter++;
							printIteration($arr, $counter);
						}
						$arr[$j+$k] = $val; // вставляем текущий элемент на свое место
						$counter++;
						printIteration($arr, $counter);
					}
					$counter++;
					printIteration($arr, $counter);
				}
				// выводим сообщение о завершении сортировки
				echo 'Сортировка завершена, проведено '.$counter.' итераций. ';
				return $arr;
			}

			function quickSort(&$arr, $left, $right, $counter){
				$l = $left; // копируем переменные для манипуляции
				$r = $right;
				$point = $arr[floor(($left+$right) / 2)]; // вычисляем опорную точку
				do{ // цикл сортировки массива
					// сдвигаем правую границу влево до тех пор, пока не найден элемент массива равный опорному
					while ($arr[$r] > $point ){
						$r--;
						$counter++;
						printIteration($arr, $counter);
					}
					// сдвигаем левую границу вправо до тех пор, пока не найден элемент массива равный опорному
					while ($arr[$l] < $point){
						$l++;
						$counter++;
						printIteration($arr, $counter);
					}
					
					if ($l <= $r){ // если левая и правая граница не пересекаются
						// меняем текущие элементы местами
						$temp = $arr[$l];
						$arr[$l] = $arr[$r];
						$arr[$r] = $temp;
						$l++; // сдвигаем границы далее
						$r--;
					}
					$counter++;
					printIteration($arr, $counter);
				} while ($l <= $r); // продолжаем цикл пока границы не пересекутся

				if ($r > $left) // если присутствует левая част массива
					$counter = quickSort($arr, $left, $r, $counter); // сортируем ее
				if ($l < $right) // если присутствует правая часть массива
					$counter = quickSort($arr, $l, $right, $counter); // сортируем ее
				return $counter;	
			}

			function printIteration($arr, $counter){
				echo "Итерация $counter. Текущее значение массива: ";
				foreach ($arr as $key => $value) {
					if ($key == count($arr) - 1){
						echo $value;
						break;
					}
					echo "$value, ";
				}
				echo "<br>";
			}
		?>
    </main>
    <footer>

    </footer>
</body>
</html>

