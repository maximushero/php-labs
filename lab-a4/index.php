<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Хоцевич М. В. 181-322 lab-a2</title>
</head>
<body>
    <header>

    </header>
    <main>
        <div class="container">
            <?php
                function getTR($data, $column){
                    $arr = explode( '*', $data ); // разбиваем строку в массив
                    $ret = '<tr>'; // начинаем тег строки таблицы
                    for($i=0; $i<$column ; $i++) // цикл по всем ячейкам таблицы
                        $ret .= '<td>'.$arr[$i].'</td>'; // добавляем ячейкам тег
                    return $ret.'</tr>'; // возвращаем строку таблицы    
                }
                function outTable($structure, $column){
                    $strings = explode( '#', $structure ); // разбиваем структуру на строки
                    $datas=''; // итоговый HTML-код строк
                    for($i=0; $i<count($strings); $i++ ){
                        if ($strings[$i] !== ""){
                            $datas .= getTR($strings[$i], $column); // добавляем код строки в итоговый
                        } else {
                            echo "В таблице нет строк с ячейкам";
                            return;
                        }
                    }
                    if($datas) // если код строк определен
                        echo '<table>'.$datas.'</table>'; // выводим таблицу
                    else // иначе
                        echo 'В таблице нет строк '; // выводим предупреждение
                }
                $column = 5;
                $structure=array(
                    'C1*C2#C3*C4#C3*C5',
                    'C7*C8*C9#C10*C11*C12',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18#',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    'C13*C14*C15#C16*C17*C18',
                    ''
                );
                if ($column <= 0){
                    echo "Неправильное число колонок";
                } else{
                    for($i=0; $i<count($structure); $i++){// для всех элементов массива
                    echo "<div class='table'><h2>Таблица№".($i + 1)."</h2>";
                    outTable($structure[$i], $column);
                    echo "</div>";
                    }  
                }
                
            ?>
        </div>
    </main>
    <footer>

    </footer>
</body>
</html>