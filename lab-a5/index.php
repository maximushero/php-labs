<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Хоцевич М. В. 181-322 lab-a5</title>
</head>
<body>
    <header>
        <div class="container">
            <div id="main_menu">
                <?php
                    echo '<a href="?html_type=TABLE'; // начало ссылки ТАБЛИЧНАЯ ФОРМА
                    if( isset($_GET['content']) ) // если параметр content был передан в скрипт
                        echo '&content='.$_GET['content']; // добавляем в ссылку второй параметр
                    echo '"'; // завершаем формирование адреса ссылки и закрываем кавычку
                    // если в скрипт был передан параметр html_type и параметр равен TABLE
                    if( array_key_exists('html_type', $_GET) && $_GET['html_type'] == 'TABLE' )
                        echo ' class="selected"'; // ссылка выделяется через CSS-класс
                    echo '>Табличная форма</a>'; // конец ссылки ТАБЛИЧНАЯ ФОРМА

                    echo '<a href="?html_type=DIV'; // начало ссылки БЛОКОВАЯ ФОРМА
                    if( isset($_GET['content']) ) // если параметр content был передан в скрипт
                        echo '&content='.$_GET['content']; // добавляем в ссылку второй параметр
                    echo '"'; // завершаем формирование адреса ссылки и закрываем кавычку
                    // если в скрипт был передан параметр html_type и параметр равен DIV
                    if( array_key_exists('html_type', $_GET) && $_GET['html_type'] == 'DIV' )
                        echo ' class="selected"'; // ссылка выделяется через CSS-класс
                    echo '>Блочная форма</a>'; // конец ссылки БЛОКОВАЯ ФОРМА
                ?>   
            </div>            
        </div>
    </header>
    <main>
        <div class="container">
            <div id="product_menu">
                <?php
                    echo '<a href="?html_type='.$_GET['html_type'].'"'; // начало ссылки ВСЯ ТАБЛИЦА УМНОЖНЕНИЯ
                    if( !isset($_GET['content']) ) // если в скрипт НЕ был передан параметр content
                        echo ' class="selected"'; // ссылка выделяется через CSS-класс
                    echo '>Вся таблица умножения</a>'; // конец ссылки

                    for( $i=2; $i<=9; $i++ ){ // цикл со счетчиком от 2 до 9 включительно
                        echo '<a href="?content='.$i; // начало ссылки
                        if( isset($_GET['html_type']) ) // если параметр content был передан в скрипт
                            echo '&html_type='.$_GET['html_type']; // начало ссылки
                        echo '"';
                        // если в скрипт был передан параметр content
                        // и параметр равен значению счетчика
                        if( isset($_GET['content']) && $_GET['content']==$i )
                            echo ' class="selected"'; // ссылка выделяется через CSS-класс
                        echo '>Таблица умножения на '.$i.'</a>'; // конец ссылки
                    }
                ?>
            </div>
            <div class="multiTable">
                <?php
                    // функция ВЫВОДИТ ТАБЛИЦУ УМНОЖЕНИЯ В ТАБЛИЧНОЙ ФОРМЕ
                    function outTableForm(){
                        echo "<table>";
                        if( !isset($_GET['content']) ){
                            for($i=2; $i<10; $i++){
                                echo "<tr>";
                                outRow( $i ); // вызывем функцию, формирующую содержание
                                // столбца умножения на $i (на 4, если $i==4)
                                echo "</tr>";
                            }
                        } else {
                            echo "<tr class='ttSingleRow'>";
                            outRow( $_GET['content'] ); // выводим выбранный в меню столбец
                            echo "</tr>";
                        }
                        echo "</table>";

                    }
                    // функция ВЫВОДИТ ТАБЛИЦУ УМНОЖЕНИЯ В БЛОЧНОЙ ФОРМЕ
                    function outDivForm (){
                        // если параметр content не был передан в программу
                        if( !isset($_GET['content']) ){
                            for($i=2; $i<10; $i++){
                                echo '<div class="ttRow">'; // оформляем таблицу в блочной форме
                                outRow( $i ); // вызывем функцию, формирующую содержание
                                // столбца умножения на $i (на 4, если $i==4)
                                echo '</div>';
                            }
                        } else {
                            echo '<div class="ttSingleRow">'; // оформляем таблицу в блочной форме
                            outRow( $_GET['content'] ); // выводим выбранный в меню столбец
                            echo '</div>';
                        }
                    }

                    // функция ВЫВОДИТ СТОЛБЕЦ ТАБЛИЦЫ УМНОЖЕНИЯ
                    function outRow ( $n ){
                        for($i=2; $i<=9; $i++){// цикл со счетчиком от 2 до 9.
                            if (!isset($_GET['html_type']) || $_GET['html_type'] == 'TABLE' ) {
                                echo "<td>";
                                outNumAsLink($n);
                                echo " * ";
                                outNumAsLink($i);
                                echo " = ";
                                outNumAsLink($i * $n);
                                echo "</td>";
                            } else {
                                outNumAsLink($n);
                                echo '* ';
                                outNumAsLink($i);
                                echo '= ';
                                outNumAsLink($i*$n);
                                echo '<br>';    
                            }
                        }
                    }
                    function outNumAsLink($x){
                        if( $x<=9 )
                            echo '<a href="?content='.$x.'">'.$x.' </a>';
                        else
                            echo $x;
                    }

                    if (!isset($_GET['html_type']) || $_GET['html_type'] == 'TABLE' )
                        outTableForm(); // выводим таблицу умножения в табличной форме
                    else
                        outDivForm(); // выводим таблицу умножения в блочной форме
                ?>
            </div>      
        </div>
    </main>
    <footer>
        <div class="container">
            <?php
                if( !isset($_GET['html_type']) || $_GET['html_type']== 'TABLE' )
                    $s='Табличная верстка. '; // строка с информацией
                else
                    $s='Блочная верстка. ';
                if( !isset($_GET['content']) )
                    $s .='Таблица умножения полностью. ';
                else
                    $s .='Столбец таблицы умножения на '.$_GET['content']. '. ';
                echo $s.date('d.Y.M h:i:s');
            ?>
        </div>
    </footer>
</body>
</html>