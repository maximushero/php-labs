<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles.css">
    <title>
        Хоцевич М.В. 181-322. А-2. Циклические алгоритмы. Условия в алгоритмах.
        Табулирование функций.
    </title>
</head>
<body>
    <header>

    </header>
    <main>
        <?php
            $x = -100; // начальное значение аргумента
            $encounting = 10000; // количество вычисляемых значений
            $step = 1; // шаг изменения аргумента
            $min_value = 2; // минимальное значение, останавливающее вычисления
            $max_value = 7000; // максимальное значение, останавливающее вычисления
            $type = 'D'; // тип верстки
            $f_array = array();
            $middle = null;

            if ($type == 'B'){
                echo '<ul>';
            }else if ($type == 'C'){
                echo '<ol>'; 
            }else if ($type == 'D'){
                echo '<table border="1">'; 
            }

            for( $i=0; $i < $encounting; $i++, $x += $step){ // цикл с заданным количеством итераций
                if( $x <= 10 ){ // если аргумент меньше или равен 10
                    $f = 10 * $x - 5;  // вычисляем функцию
                }    
                else if( $x > 10 && $x < 20 ) { // если аргумент меньше 20
                    $f = ($x + 3) * $x**2; // вычисляем функцию
                } 
                else if ($x == 25){
                    $f = 'error';
                }
                else{
                    $f = (3 / ($x - 25)) + 2; // вычисляем функцию
                }

                $f = round($f, 3);

                array_push($f_array, $f);

                // if( $f >= $max_value || $f < $min_value ) // если вышли за рамки диапазона
                //     break; // закончить цикл досрочно


                if ($type == 'A'){
                    echo "f($x) = $f"; // выводим аргумент и значение функции
                    if( $i < $encounting-1 ) // если это не последняя итерация цикла
                        echo '<br>';
                }else if ($type == 'B' || $type == 'C'){
                        echo "<li>f($x) = $f</li>";
                }else if($type == 'D'){
                    $num = $i + 1;
                    echo "<tr><td>$num</td><td>$x</td><td>$f</td></tr>";
                }else if($type == 'E'){
                    echo "<div class='E'>f($x) = $f</div>";
                }
                
                $middle += $f;
                
            }

            if ($type == 'B'){
                echo '</ul>';
            }else if ($type == 'C'){
                echo '</ol>'; 
            }else if ($type == 'D'){
                echo '</table>'; 
            }

            
            $middle = round($middle, 3);
            $min = min($f_array);
            $max = max($f_array);
            echo "Сумма всех значений - $middle<br>";
            echo "Минимальное значение функции - $min<br>";
            echo "Максимальное значение функции - $max<br>";
            $middle /= $encounting;
            $middle = round($middle, 3);
            echo "Среднее арифметическое значение функции - $middle<br>";
        ?>
    </main>
    <footer>
        <?php
            echo "Тип верстки: $type"
        ?>
    </footer>
</body>
</html>